<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class UsersController extends Controller
{
     public function index(){

     	$users = User::all();

    	return view('users.index', compact('users'));

    }

    public function create(){
    	return view('users.create');
    }

    public function store(CreateUserRequest $request){

		User::create($request->all());

		return redirect('/users');
    }

    public function show($id){
    	
        $user = User::findOrFail($id);

        return view('users.show', compact('user'));

    }

    public function edit($id){

        $user = User::findOrFail($id);

        return view('users.edit', compact('user'));

    }

    public function delete($id){

        $user = User::findOrFail($id);

        return view('users.destroy', compact('user'));

    }

    public function update(CreateUserRequest $request, $id){

        $user = User::findOrFail($id);

        $user->update($request->all());

        return redirect('/users/');

    }

    public function destroy($id){
        User::findOrFail($id)->delete();

        return redirect('/users/');
    }

}
