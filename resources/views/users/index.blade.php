@extends('layouts.app')



@section('content')

	<!-- <ul>
		@foreach($users as $user)
			<li><a href="{{route('users.show', $user->id)}}">{{$user->name}}</a></li>
		@endforeach
	</ul> -->

	<table class="table">
	  	<thead>
	    	<tr>
			    <th scope="col"><b>Nº</b></th>
			    <th scope="col">Nome</th>
			    <th scope="col">E-mail</th>
			    <th scope="col">Editar</th>
			    <th scope="col">Excluir</th>
	    	</tr>
	  	</thead>
	  	<tbody>
	  		<?php $count = 0; ?>
	  		@foreach($users as $user)
	  			<?php $count = $count + 1; ?>
		  		<tr>
		      		<th scope="row">{{$user->id}}</th>
		      		<td>{{$user->name}}</td>
		      		<td>{{$user->email}}</td>
		      		<td><a href="{{route('users.edit', $user->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
		      		<td>
		      			{!! Form::open(['method'=>'DELETE', 'action'=>['UsersController@destroy', $user->id]]) !!}

							{{csrf_field()}}

							<button type="submit" class="fa fa-times"><!-- <i class="fa fa-times" aria-hidden="true"></i> --></button>

						{!! Form::close() !!}
		      		</td>
		    	</tr>
			@endforeach
			@if($count == 0)
				<tr><td colspan="5" align="center">Nenhum usuário encontrado!</td></tr>
			@endif
	  	<tbody>
	</table>
@endsection


@yield('footer')
