@extends('layouts.app')



@section('content')



	<h1>Editar: {{$user->name}}</h1>

	{!! Form::model($user, ['method'=>'PATCH', 'action'=>['UsersController@update', $user->id]]) !!}

		<div class="form-group">

			{!! Form::label('name', 'Nome:') !!}
			{!! Form::text('name', null, ['class'=>'form-control'])!!}
			<!-- <input type="text" name="title" placeholder="Enter title"> -->
		</div>

		<div class="form-group">

			{!! Form::label('email', 'Email:') !!}
			{!! Form::email('email', null, ['class'=>'form-control'])!!}
			<!-- <input type="text" name="title" placeholder="Enter title"> -->
		</div>

		<div class="form-group">

			{!! Form::label('password', 'Senha:') !!}
			<!-- {!! Form::password('password', null, ['value'=>'$user->name'],['class'=>'form-control'])!!} -->
			<input type="password" class="form-control" name="password"  value="{{$user->password}}" >

		</div>
		
		{{csrf_field()}}


		{!! Form::submit('Editar Usuário', ['class'=>'btn btn-primary']) !!}

		<!-- <input type="submit" name="submit"> -->
	
	{!! Form::close() !!}


	@if(isset($errors))
		<br>
		<br>
		@foreach($errors->all() as $error)
					<li>{{$error}}</li>
		@endforeach
	@endif



@yield('footer')

@endsection