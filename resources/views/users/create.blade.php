@extends('layouts.app')



@section('content')

	<h1>Criar Usuário</h1>

	<!-- <form method="post" action="../posts"> -->

	{!! Form::open(['method'=>'POST', 'action'=>'UsersController@store']) !!}

		<div class="form-group">

			{!! Form::label('name', 'Nome:') !!}
			{!! Form::text('name', null, ['class'=>'form-control'])!!}
			<!-- <input type="text" name="title" placeholder="Enter title"> -->
		</div>

		<div class="form-group">

			{!! Form::label('email', 'Email:') !!}
			{!! Form::text('email', null, ['class'=>'form-control'])!!}
			<!-- <input type="text" name="title" placeholder="Enter title"> -->
		</div>

		<div class="form-group">

			{!! Form::label('password', 'Senha:') !!}
			<!-- {!! Form::password('password', null, ['class'=>'form-control'])!!} -->
			<input type="password" class="form-control" name="password">
		</div>
		
		{{csrf_field()}}

		{!! Form::submit('Criar Usuário', ['class'=>'btn btn-primary']) !!}

		<!-- <input type="submit" name="submit"> -->
	
	{!! Form::close() !!}

	@if(isset($errors))
		<br>
		<br>
		@foreach($errors->all() as $error)
					<li>{{$error}}</li>
		@endforeach
	@endif
<!-- 
	@if(count($errors) > 0)

		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>

	@endif -->

@yield('footer')

@endsection